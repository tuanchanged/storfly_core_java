package com.virtium.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
	
	Properties configFile;
		
	public Config(String configFilePath)
	{
		InputStream input = null;
		configFile = new Properties();
		try {
			input = new FileInputStream(configFilePath);			
			configFile.load(input);					
		} 
		catch (IOException e) 
		{
			System.out.println(String.format("ERROR: Unable load the %s config file, due to exception %s", configFilePath, e.getMessage()));
		} 
		finally 
		{
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					System.out.println(String.format("ERROR: Unable close the %s config file, due to exception %s", configFilePath, e.getMessage()));
				}
			}
		}
	}
	
	public String getProperty(String key)
	{
		String value = null;
		value = this.configFile.getProperty(key);
		return value;
	}
	
	

}
