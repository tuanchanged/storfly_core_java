package com.virtium.utilities;

import java.io.File;
import java.util.List;

public class LogFileProcessing {
	
	public void getAllFileNameInDir(String folderPath, List<String> listOfFileNames)
	{		
		try {
			File root = new File(folderPath);			
			File[] list = root.listFiles();
			
			if(list == null)
			{
				return;
			}
			
			for ( File f : list ) {
	            if ( f.isDirectory() ) {
	            	getAllFileNameInDir( f.getAbsolutePath() , listOfFileNames);
	            }
	            else {	            	
	                listOfFileNames.add(f.getAbsolutePath());
	            }
	        }
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to get all filenames in the %s folder , due to exception %s", folderPath, e.getMessage()));
			
		}
				
	}

}
