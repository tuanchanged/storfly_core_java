package com.virtium.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.virtium.cmd.CommandLine;

public class SevenZip {

	/*******************************************************
	 * Decompress zip file
	 * 
	 * @author tuan.nguyen
	 * @param: iZipFilePath,
	 *             iOutputFolder
	 **/
	public void decompressZipFile(String iZipFilePath, String iOutputFolder) throws Exception {

		String decompressCommandString = String.format("7z x %s -o%s -y", iZipFilePath, iOutputFolder);

		CommandLine cmd = new CommandLine();
		String errorMessage = cmd.call(decompressCommandString).getErrorMessage();

		if (errorMessage != "") {
			System.out.println("-----> :" + errorMessage + ":");
			throw new Exception(errorMessage);
		}
	}

	/*******************************************************
	 * Read content of the zip file
	 * 
	 * @author tuan.nguyen
	 * @param: iZipFilePath,
	 *             iOutputFolder
	 * @throws Exception
	 **/

	public String readContentOfZipFile(String iZipFilePath) throws Exception {

		if (!(new File(iZipFilePath)).exists()) {
			throw new Exception("The zip file path " + iZipFilePath + " is not existed!");
		}

		String outputDir = System.getProperty("user.dir") + "/dirtemp";

		// Unzip
		decompressZipFile(iZipFilePath, outputDir);

		// Read content
		File file = new File(outputDir);
		File[] files = file.listFiles();
		FileInputStream fileInputStream = new FileInputStream(files[0]);
		String content = IOUtils.toString(fileInputStream, StandardCharsets.UTF_8.name());
		fileInputStream.close();

		// Delete file after reading content
		FileUtils.deleteDirectory(new File(outputDir));

		return content;
	}
}
