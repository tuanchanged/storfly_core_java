package com.virtium.cmd;

public class CommandLineResult {
	private boolean m_IsSeccsess;
	private String m_OutputMessage;
	private String m_ErrorMessage;

	public CommandLineResult(boolean iIsSeccsess, String iOutputMessage, String iErrorMessage) {
		m_IsSeccsess = iIsSeccsess;
		m_OutputMessage = iOutputMessage;
		m_ErrorMessage = iErrorMessage;
	}

	public boolean getIsSeccsess() {
		return m_IsSeccsess;
	}

	public String getOutputMessage() {
		return m_OutputMessage;
	}

	public String getErrorMessage() {
		return m_ErrorMessage;
	}
}
