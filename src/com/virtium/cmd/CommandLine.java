package com.virtium.cmd;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CommandLine {
	public CommandLineResult call(String iCommand) throws Exception {
		/*
		 * Run command line on the current process.
		 *
		 * Returns: boolean: True if successful, False otherwise.
		 */
		System.out.println(String.format("Command Line Call [%s]", iCommand));
		Process process = Runtime.getRuntime().exec(iCommand);
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		// Read the output from the command
		String line;
		String outputMessage = "";
		while ((line = stdInput.readLine()) != null) {
			outputMessage += line + "\n";
			System.out.println(line);
		}

		// Read any errors from the attempted command
		String errorMessage = "";
		boolean isSeccsess = true;
		while ((line = stdError.readLine()) != null) {
			errorMessage += line + "\n";
		}

		if (stdError.readLine() != null) {
			isSeccsess = false;
		}

		CommandLineResult cmdResult = new CommandLineResult(isSeccsess, outputMessage, errorMessage);
		return cmdResult;
	}
}
