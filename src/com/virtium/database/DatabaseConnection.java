package com.virtium.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseConnection {
	
	private Connection conn = null;
	private String dbServerAddress = null;
	private String port = null;
	private String dbName = null;
	private String driverName = null;
	private String username = null;
	private String password = null;
	
	public DatabaseConnection(String dbServerAddress, String username, String password, String port, String dbName, String driverName)
	{
		this.dbServerAddress = dbServerAddress;
		this.port = port;
		this.dbName = dbName;
		this.driverName = driverName;
		this.username = username;
		this.password = password;
	}
	
	public boolean connectionDB()
	{
		Boolean result = false;
		String connectionUrl = "jdbc:sqlserver://" +dbServerAddress + ":" + port + ";databaseName=" + dbName;
		
		try {
			Class.forName(driverName);
			conn = DriverManager.getConnection(connectionUrl,username, password);
			result = true;
		} catch (ClassNotFoundException ex) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to find class for the %s driver, due to exception %s", driverName, ex.getMessage()));
			result = false;
		}
		catch(SQLException e)
		{
			System.out.println(String.format("ERROR: Unable to connect to SQL server, due to exception %s", e.getMessage()));
			result = false;
		}
		return result;
	}
	
	public boolean closeDBConnection()
	{		
		try {
			if(conn != null)
			{
				conn.close();
			}
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable close database, due to exception %s", e.getMessage()));
			return false;
		}
		
	}
	
	public boolean execute(String queryString)
	{
		boolean result = false;
		try {
			Statement stmt = conn.createStatement();
			result = stmt.execute(queryString);		
			result = true;			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to execute the %s string, due to exception %s", queryString, e.getMessage()));		
			result = false;
		}
		return result;
	}
	
	public ResultSet  executeQuery(String queryString)
	{
		ResultSet rs = null;
		try {
			Statement stmt = conn.createStatement();
			rs =stmt.executeQuery(queryString);						
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to execute query the %s string, due to exception %s", queryString, e.getMessage()));
		}
		return rs;
	}
	
	
	
	
	
	
	
	
	
	

}
