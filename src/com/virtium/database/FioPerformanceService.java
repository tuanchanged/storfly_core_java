package com.virtium.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.virtium.dao.BandWidthPerformanceDAOImpl;
import com.virtium.dao.CategoryDAOImpl;
import com.virtium.dao.ClatPerformanceDAOImpl;
import com.virtium.dao.IopsPerformanceDAOImpl;
import com.virtium.dao.LatencyPerformanceDAOImpl;
import com.virtium.dao.ReleaseCandidateDAOImpl;
import com.virtium.dao.SlatPerformanceDAOImpl;
import com.virtium.dao.TestDAOImpl;
import com.virtium.model.BandWidthPerformance;
import com.virtium.model.Category;
import com.virtium.model.ClatPerformance;
import com.virtium.model.IopsPerformance;
import com.virtium.model.LatencyPerformance;
import com.virtium.model.ReleaseCandidate;
import com.virtium.model.SlatPerformance;
import com.virtium.model.Test;
import com.virtium.utilities.Config;
import com.virtium.utilities.LogFileProcessing;

public class FioPerformanceService {
	
	public boolean transferSlatDataToDB(DatabaseConnection db, String releaseCandidateId, String slatLog)
	{
		boolean result = false;
		String id = null;
		Integer time = null;
		Integer slatValue = null;
		Integer readWrite = null;
		BufferedReader br = null;
		String cvsSplitBy = ",";
		String line = "";
		SlatPerformanceDAOImpl slatDAOImpl = new SlatPerformanceDAOImpl(db);
		
		try {
			br = new BufferedReader(new FileReader(slatLog));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] slatData = line.split(cvsSplitBy);
				time = 	Integer.parseInt(slatData[0].trim()) ;
				slatValue = Integer.parseInt(slatData[1].trim());
				readWrite = Integer.parseInt(slatData[2].trim());
				id = UUID.randomUUID().toString();
				SlatPerformance slatPerformance = new SlatPerformance(id, time, slatValue, readWrite, releaseCandidateId);
				result = slatDAOImpl.insertSlatPerformance(slatPerformance);
			}
			
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to find the %s slat log, due to exception %s", slatLog, e.getMessage()));
		}
		catch (IOException  e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to read the %s slat log, due to exception %s", slatLog, e.getMessage()));
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(String.format("ERROR: Unable to close the %s slat log, due to exception %s", slatLog, e.getMessage()));
				}
			}
		}
		
		return result;
	}
	public boolean transferLatencyDataToDB(DatabaseConnection db, String releaseCandidateId, String latencyLog)
	{
		boolean result = false;
		String id = null;
		Integer time = null;
		Integer latencyValue = null;
		Integer readWrite = null;
		BufferedReader br = null;
		String cvsSplitBy = ",";
		String line = "";
		LatencyPerformanceDAOImpl latencyDAOImpl = new LatencyPerformanceDAOImpl(db);
		
		try {
			br = new BufferedReader(new FileReader(latencyLog));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] latencyData = line.split(cvsSplitBy);
				time = 	Integer.parseInt(latencyData[0].trim()) ;
				latencyValue = Integer.parseInt(latencyData[1].trim()) ;
				readWrite = Integer.parseInt(latencyData[2].trim()) ;
				id = UUID.randomUUID().toString();
				LatencyPerformance latencyPerformance = new LatencyPerformance(id, time, latencyValue, readWrite, releaseCandidateId);
				result = latencyDAOImpl.insertLatencyPerformance(latencyPerformance);
			}
			
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to find the %s latency log, due to exception %s", latencyLog, e.getMessage()));
		}
		catch (IOException  e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to read the %s latency log, due to exception %s", latencyLog, e.getMessage()));
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(String.format("ERROR: Unable to close the %s latency log, due to exception %s", latencyLog, e.getMessage()));
				}
			}
		}
		
		return result;
	}
	public boolean transferIopsDataToDB(DatabaseConnection db, String releaseCandidateId, String iopsLog)
	{
		boolean result = false;
		String id = null;
		Integer time = null;
		Integer iopsValue = null;
		Integer readWrite = null;
		BufferedReader br = null;
		String cvsSplitBy = ",";
		String line = "";
		IopsPerformanceDAOImpl iopsDAOImpl = new IopsPerformanceDAOImpl(db);
		
		try {
			br = new BufferedReader(new FileReader(iopsLog));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] iopsData = line.split(cvsSplitBy);
				time = 	Integer.parseInt(iopsData[0].trim()) ;
				iopsValue = Integer.parseInt(iopsData[1].trim()) ;
				readWrite = Integer.parseInt(iopsData[2].trim()) ;
				id = UUID.randomUUID().toString();
				IopsPerformance iopsPerformance = new IopsPerformance(id, time, iopsValue, readWrite, releaseCandidateId);
				result = iopsDAOImpl.insertIopsPerformance(iopsPerformance);
			}
			
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to find the %s iops log, due to exception %s", iopsLog, e.getMessage()));
		}
		catch (IOException  e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to read the %s iops log, due to exception %s", iopsLog, e.getMessage()));
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(String.format("ERROR: Unable to close the %s iops log, due to exception %s", iopsLog, e.getMessage()));
				}
			}
		}
		
		return result;
	}
	public boolean transferClatDataToDB(DatabaseConnection db, String releaseCandidateId, String clatLog)
	{
		boolean result = false;
		String id = null;
		Integer time = null;
		Integer clatValue = null;
		Integer readWrite = null;
		BufferedReader br = null;
		String cvsSplitBy = ",";
		String line = "";
		ClatPerformanceDAOImpl clatDAOImpl = new ClatPerformanceDAOImpl(db);
		
		try {
			br = new BufferedReader(new FileReader(clatLog));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] clatData = line.split(cvsSplitBy);
				time = 	Integer.parseInt(clatData[0].trim()) ;
				clatValue = Integer.parseInt(clatData[1].trim()) ;
				readWrite = Integer.parseInt(clatData[2].trim()) ;
				id = UUID.randomUUID().toString();
				ClatPerformance clatPerformance = new ClatPerformance(id, time, clatValue, readWrite, releaseCandidateId);
				result = clatDAOImpl.insertClatPerformance(clatPerformance);
			}
			
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to find the %s clat log, due to exception %s", clatLog, e.getMessage()));
		}
		catch (IOException  e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to read the %s clat log, due to exception %s", clatLog, e.getMessage()));
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(String.format("ERROR: Unable to close the %s clat log, due to exception %s", clatLog, e.getMessage()));
				}
			}
		}
		
		return result;
	}
	public boolean transferBandWidthDataToDB(DatabaseConnection db, String releaseCandidateId, String bandWidthLog)
	{
		boolean result = false;
		String id = null;
		Integer time = null;
		Integer bandWidthValue = null;
		Integer readWrite = null;
		BufferedReader br = null;
		String cvsSplitBy = ",";
		String line = "";
		BandWidthPerformanceDAOImpl bandWidthDAOImpl = new BandWidthPerformanceDAOImpl(db);
		
		try {
			br = new BufferedReader(new FileReader(bandWidthLog));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] bandWidthData = line.split(cvsSplitBy);					
				time = 	Integer.parseInt(bandWidthData[0].trim());				
				bandWidthValue = Integer.parseInt(bandWidthData[1].trim());				
				readWrite = Integer.parseInt(bandWidthData[2].trim());											
				id = UUID.randomUUID().toString();
				BandWidthPerformance bandWidthPerformance = new BandWidthPerformance(id, time, bandWidthValue, readWrite, releaseCandidateId);
				result = bandWidthDAOImpl.insertBandWidthPerformance(bandWidthPerformance);
			}
			
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to find the %s bandwidth log, due to exception %s", bandWidthLog, e.getMessage()));
		}
		catch (IOException  e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to read the %s bandwidth log, due to exception %s", bandWidthLog, e.getMessage()));
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(String.format("ERROR: Unable to close the %s bandwidth log, due to exception %s", bandWidthLog, e.getMessage()));
				}
			}
		}
		
		return result;
	}
	
	
	public boolean transferFioPerformanceDataToDB(String fioLogDir, String versionName)
	{
		boolean result = false;
		String releaseCandidateId = null;
		String categoryName = null;
		String testId = null;
		String categoryId = null;
		String id = null;
		int pos = 0;
		List<String> listOfFioLogs = null;			
		Config config = new Config("configs/database_config.properties");
		String dbServerAddress =  config.getProperty("db_server_address");
		String username = config.getProperty("username");
		String password = config.getProperty("password");
		String port = config.getProperty("port");
		String dbName = config.getProperty("db_name");
		String driverName = config.getProperty("driver_name");
		
		
				
		DatabaseConnection db = new DatabaseConnection(dbServerAddress, username, password, port, dbName, driverName);
		if(db.connectionDB() == false)
		{
			System.out.println(String.format("Connect to database: FAILED"));
			return false;
		}
		else
		{
			listOfFioLogs = new ArrayList<String>();
			LogFileProcessing fileLogProcessing = new LogFileProcessing();
			fileLogProcessing.getAllFileNameInDir(fioLogDir, listOfFioLogs);
			
			
			for(int index = 0; index < listOfFioLogs.size(); index++)
			{														
				if(listOfFioLogs.get(index).contains("_bw"))
				{	
					categoryName = "BandWidth";
					//get CategoryId
					CategoryDAOImpl categoryDAOImpl = new CategoryDAOImpl(db);
					Category category = new Category();
					category = categoryDAOImpl.getCategory(categoryName);
					if(category != null)
					{
						categoryId = category.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						category = new Category(id, categoryName);
						result =  categoryDAOImpl.insertCategory(category);
						if(result == false)
						{
							return false;
						}
						else
						{
							categoryId = id;
						}
					}
					
					//Get TestId
					File filePath = new File(listOfFioLogs.get(index));
					String fileName = null;
					TestDAOImpl testDAOImpl = new TestDAOImpl(db);
					Test test = new Test();
					fileName = filePath.getName();								
					pos = fileName.indexOf(".1.log");
					System.out.println("TestName: "+fileName.substring(0, pos));
					test = testDAOImpl.getTest(fileName.substring(0, pos), categoryId);
					if(test != null)
					{
						testId = test.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						test = new Test(id, fileName.substring(0, pos), categoryId);
						result = testDAOImpl.insertTest(test);
						if(result == false)
						{
							return false;
						}
						else
						{
							testId = id;
						}
					}
					
					//Get ReleaseCandidateId				
					ReleaseCandidateDAOImpl releaseCandidateDAOImpl = new ReleaseCandidateDAOImpl(db);
					ReleaseCandidate releaseCandidate = new ReleaseCandidate();
					releaseCandidate = releaseCandidateDAOImpl.getReleaseCandidate(versionName, testId);
					if(releaseCandidate != null)
					{	
						System.out.println(String.format("This %s version is imported to database. Please import another version", versionName));
						return false;
					}
					else
					{
						id = UUID.randomUUID().toString();
						releaseCandidate = new ReleaseCandidate(id, versionName, testId);
						result = releaseCandidateDAOImpl.insertReleaseCandidate(releaseCandidate);
						if(result == false)
						{
							return false;
						}
						else
						{
							releaseCandidateId = id;
						}
						
					}
					
					transferBandWidthDataToDB(db, releaseCandidateId, listOfFioLogs.get(index));
				}				
				else if(listOfFioLogs.get(index).contains("_clat"))
				{
					categoryName = "Clat";
					//get CategoryId
					CategoryDAOImpl categoryDAOImpl = new CategoryDAOImpl(db);
					Category category = new Category();
					category = categoryDAOImpl.getCategory(categoryName);
					if(category != null)
					{
						categoryId = category.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						category = new Category(id, categoryName);
						result =  categoryDAOImpl.insertCategory(category);
						if(result == false)
						{
							return false;
						}
						else
						{
							categoryId = id;
						}
					}
					
					//Get TestId
					File filePath = new File(listOfFioLogs.get(index));
					String fileName = null;
					TestDAOImpl testDAOImpl = new TestDAOImpl(db);
					Test test = new Test();
					fileName = filePath.getName();								
					pos = fileName.indexOf(".1.log");
					System.out.println("TestName: "+fileName.substring(0, pos));
					test = testDAOImpl.getTest(fileName.substring(0, pos), categoryId);
					if(test != null)
					{
						testId = test.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						test = new Test(id, fileName.substring(0, pos), categoryId);
						result = testDAOImpl.insertTest(test);
						if(result == false)
						{
							return false;
						}
						else
						{
							testId = id;
						}
					}
					
					//Get ReleaseCandidateId				
					ReleaseCandidateDAOImpl releaseCandidateDAOImpl = new ReleaseCandidateDAOImpl(db);
					ReleaseCandidate releaseCandidate = new ReleaseCandidate();
					releaseCandidate = releaseCandidateDAOImpl.getReleaseCandidate(versionName, testId);
					if(releaseCandidate != null)
					{	
						System.out.println(String.format("This %s version is imported to database. Please import another version", versionName));
						return false;
					}
					else
					{
						id = UUID.randomUUID().toString();
						releaseCandidate = new ReleaseCandidate(id, versionName, testId);
						result = releaseCandidateDAOImpl.insertReleaseCandidate(releaseCandidate);
						if(result == false)
						{
							return false;
						}
						else
						{
							releaseCandidateId = id;
						}
						
					}
					transferClatDataToDB(db, releaseCandidateId, listOfFioLogs.get(index));
				}
				else if(listOfFioLogs.get(index).contains("_iops"))
				{
					categoryName = "Iops";
					//get CategoryId
					CategoryDAOImpl categoryDAOImpl = new CategoryDAOImpl(db);
					Category category = new Category();
					category = categoryDAOImpl.getCategory(categoryName);
					if(category != null)
					{
						categoryId = category.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						category = new Category(id, categoryName);
						result =  categoryDAOImpl.insertCategory(category);
						if(result == false)
						{
							return false;
						}
						else
						{
							categoryId = id;
						}
					}
					
					//Get TestId
					File filePath = new File(listOfFioLogs.get(index));
					String fileName = null;
					TestDAOImpl testDAOImpl = new TestDAOImpl(db);
					Test test = new Test();
					fileName = filePath.getName();								
					pos = fileName.indexOf(".1.log");
					System.out.println("TestName: "+fileName.substring(0, pos));
					test = testDAOImpl.getTest(fileName.substring(0, pos), categoryId);
					if(test != null)
					{
						testId = test.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						test = new Test(id, fileName.substring(0, pos), categoryId);
						result = testDAOImpl.insertTest(test);
						if(result == false)
						{
							return false;
						}
						else
						{
							testId = id;
						}
					}
					
					//Get ReleaseCandidateId				
					ReleaseCandidateDAOImpl releaseCandidateDAOImpl = new ReleaseCandidateDAOImpl(db);
					ReleaseCandidate releaseCandidate = new ReleaseCandidate();
					releaseCandidate = releaseCandidateDAOImpl.getReleaseCandidate(versionName, testId);
					if(releaseCandidate != null)
					{	
						System.out.println(String.format("This %s version is imported to database. Please import another version", versionName));
						return false;
					}
					else
					{
						id = UUID.randomUUID().toString();
						releaseCandidate = new ReleaseCandidate(id, versionName, testId);
						result = releaseCandidateDAOImpl.insertReleaseCandidate(releaseCandidate);
						if(result == false)
						{
							return false;
						}
						else
						{
							releaseCandidateId = id;
						}
						
					}
					transferIopsDataToDB(db, releaseCandidateId, listOfFioLogs.get(index));
				}
				else if(listOfFioLogs.get(index).contains("_lat"))
				{
					categoryName = "Latency";
					//get CategoryId
					CategoryDAOImpl categoryDAOImpl = new CategoryDAOImpl(db);
					Category category = new Category();
					category = categoryDAOImpl.getCategory(categoryName);
					if(category != null)
					{
						categoryId = category.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						category = new Category(id, categoryName);
						result =  categoryDAOImpl.insertCategory(category);
						if(result == false)
						{
							return false;
						}
						else
						{
							categoryId = id;
						}
					}
					
					//Get TestId
					File filePath = new File(listOfFioLogs.get(index));
					String fileName = null;
					TestDAOImpl testDAOImpl = new TestDAOImpl(db);
					Test test = new Test();
					fileName = filePath.getName();								
					pos = fileName.indexOf(".1.log");
					System.out.println("TestName: "+fileName.substring(0, pos));
					test = testDAOImpl.getTest(fileName.substring(0, pos), categoryId);
					if(test != null)
					{
						testId = test.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						test = new Test(id, fileName.substring(0, pos), categoryId);
						result = testDAOImpl.insertTest(test);
						if(result == false)
						{
							return false;
						}
						else
						{
							testId = id;
						}
					}
					
					//Get ReleaseCandidateId				
					ReleaseCandidateDAOImpl releaseCandidateDAOImpl = new ReleaseCandidateDAOImpl(db);
					ReleaseCandidate releaseCandidate = new ReleaseCandidate();
					releaseCandidate = releaseCandidateDAOImpl.getReleaseCandidate(versionName, testId);
					if(releaseCandidate != null)
					{	
						System.out.println(String.format("This %s version is imported to database. Please import another version", versionName));
						return false;
					}
					else
					{
						id = UUID.randomUUID().toString();
						releaseCandidate = new ReleaseCandidate(id, versionName, testId);
						result = releaseCandidateDAOImpl.insertReleaseCandidate(releaseCandidate);
						if(result == false)
						{
							return false;
						}
						else
						{
							releaseCandidateId = id;
						}
						
					}
					transferLatencyDataToDB(db, releaseCandidateId, listOfFioLogs.get(index));
				}
				else if(listOfFioLogs.get(index).contains("_slat"))
				{
					categoryName = "Slat";
					//get CategoryId
					CategoryDAOImpl categoryDAOImpl = new CategoryDAOImpl(db);
					Category category = new Category();
					category = categoryDAOImpl.getCategory(categoryName);
					if(category != null)
					{
						categoryId = category.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						category = new Category(id, categoryName);
						result =  categoryDAOImpl.insertCategory(category);
						if(result == false)
						{
							return false;
						}
						else
						{
							categoryId = id;
						}
					}
					
					//Get TestId
					File filePath = new File(listOfFioLogs.get(index));
					String fileName = null;
					TestDAOImpl testDAOImpl = new TestDAOImpl(db);
					Test test = new Test();
					fileName = filePath.getName();								
					pos = fileName.indexOf(".1.log");
					System.out.println("TestName: "+fileName.substring(0, pos));
					test = testDAOImpl.getTest(fileName.substring(0, pos), categoryId);
					if(test != null)
					{
						testId = test.getId();
					}
					else
					{
						id = UUID.randomUUID().toString();
						test = new Test(id, fileName.substring(0, pos), categoryId);
						result = testDAOImpl.insertTest(test);
						if(result == false)
						{
							return false;
						}
						else
						{
							testId = id;
						}
					}
					
					//Get ReleaseCandidateId				
					ReleaseCandidateDAOImpl releaseCandidateDAOImpl = new ReleaseCandidateDAOImpl(db);
					ReleaseCandidate releaseCandidate = new ReleaseCandidate();
					releaseCandidate = releaseCandidateDAOImpl.getReleaseCandidate(versionName, testId);
					if(releaseCandidate != null)
					{	
						System.out.println(String.format("This %s version is imported to database. Please import another version", versionName));
						return false;
					}
					else
					{
						id = UUID.randomUUID().toString();
						releaseCandidate = new ReleaseCandidate(id, versionName, testId);
						result = releaseCandidateDAOImpl.insertReleaseCandidate(releaseCandidate);
						if(result == false)
						{
							return false;
						}
						else
						{
							releaseCandidateId = id;
						}
						
					}
					transferSlatDataToDB(db, releaseCandidateId, listOfFioLogs.get(index));
				}
			}
			
		}
		return result;
	}

}
