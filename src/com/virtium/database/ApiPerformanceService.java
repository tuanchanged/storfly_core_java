package com.virtium.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.virtium.dao.ApiPerformanceDAOImpl;
import com.virtium.dao.BandWidthPerformanceDAOImpl;
import com.virtium.dao.CategoryDAOImpl;
import com.virtium.dao.ReleaseCandidateDAOImpl;
import com.virtium.dao.TestDAOImpl;
import com.virtium.model.ApiPerformance;
import com.virtium.model.BandWidthPerformance;
import com.virtium.model.Category;
import com.virtium.model.ReleaseCandidate;
import com.virtium.model.Test;
import com.virtium.utilities.Config;
import com.virtium.utilities.LogFileProcessing;

public class ApiPerformanceService {
	
	public boolean transferApiDataToDB(String apiLogDir, String versionName)
	{
		boolean result = false;
		List<String> listOfApiLogs = null;	
		String categoryId = null;
		String testId = null;
		String releaseCandidateId = null;
		String categoryName = null;
		String id = null;
		int pos = 0;
		
		Config config = new Config("configs/database_config.properties");
		String dbServerAddress =  config.getProperty("db_server_address");
		String username = config.getProperty("username");
		String password = config.getProperty("password");
		String port = config.getProperty("port");
		String dbName = config.getProperty("db_name");
		String driverName = config.getProperty("driver_name");
		
		DatabaseConnection db = new DatabaseConnection(dbServerAddress, username, password, port, dbName, driverName);
		if(db.connectionDB() == false)
		{
			System.out.println(String.format("Connect to database: FAILED"));
			return false;
		}
		else
		{
			listOfApiLogs = new ArrayList<String>();
			LogFileProcessing fileLogProcessing = new LogFileProcessing();
			fileLogProcessing.getAllFileNameInDir(apiLogDir, listOfApiLogs);
			System.out.println("Size: "+listOfApiLogs.size());
			
			for(int index = 0; index < listOfApiLogs.size(); index++)
			{
				//get CategoryId
				categoryName = "API";
				CategoryDAOImpl categoryDAOImpl = new CategoryDAOImpl(db);
				Category category = new Category();
				category = categoryDAOImpl.getCategory(categoryName);
				if(category != null)
				{
					categoryId = category.getId();
				}
				else
				{
					id = UUID.randomUUID().toString();
					category = new Category(id, categoryName);
					result =  categoryDAOImpl.insertCategory(category);
					if(result == false)
					{
						return false;
					}
					else
					{
						categoryId = id;
					}
				}
				
				//Get TestId
				File filePath = new File(listOfApiLogs.get(index));
				String fileName = null;
				TestDAOImpl testDAOImpl = new TestDAOImpl(db);
				Test test = new Test();
				fileName = filePath.getName();								
				pos = fileName.indexOf(".txt");
				System.out.println("TestName: "+fileName.substring(0, pos));
				test = testDAOImpl.getTest(fileName.substring(0, pos), categoryId);
				if(test != null)
				{
					testId = test.getId();
				}
				else
				{
					id = UUID.randomUUID().toString();
					test = new Test(id, fileName.substring(0, pos), categoryId);
					result = testDAOImpl.insertTest(test);
					if(result == false)
					{
						return false;
					}
					else
					{
						testId = id;
					}
				}
				
				//Get ReleaseCandidateId				
				ReleaseCandidateDAOImpl releaseCandidateDAOImpl = new ReleaseCandidateDAOImpl(db);
				ReleaseCandidate releaseCandidate = new ReleaseCandidate();
				releaseCandidate = releaseCandidateDAOImpl.getReleaseCandidate(versionName, testId);
				if(releaseCandidate != null)
				{	
					System.out.println(String.format("This %s version is imported to database. Please import another version", versionName));
					return false;
				}
				else
				{
					id = UUID.randomUUID().toString();
					releaseCandidate = new ReleaseCandidate(id, versionName, testId);
					result = releaseCandidateDAOImpl.insertReleaseCandidate(releaseCandidate);
					if(result == false)
					{
						return false;
					}
					else
					{
						releaseCandidateId = id;
					}
					
				}
				
				if(getApiDataFromLogAndInsertToDB(db, releaseCandidateId, listOfApiLogs.get(index)) == false)
				{
					return false;
				}
				
														
			}
		}
		return result;
	}
	
	public boolean getApiDataFromLogAndInsertToDB(DatabaseConnection db, String releaseCandidateId, String apiLog)
	{
		boolean result = false;
		int iteration = 0;
		String id = null;
		int iterationTemp = 0;
		float responseTime = 0;		
		float duration = 0;
		float durationTemp = 0;
		BufferedReader br = null;
		String cvsSplitBy = ",";
		String line = "";
		ApiPerformanceDAOImpl apiDAOImpl = new ApiPerformanceDAOImpl(db);
		try {
			br = new BufferedReader(new FileReader(apiLog));
			while ((line = br.readLine()) != null) {				
				// use comma as separator
				String[] apiData = line.split(cvsSplitBy);					
				iteration = Integer.parseInt(apiData[0].trim()) + iterationTemp;				
				responseTime = Float.parseFloat(apiData[1].trim());			
				duration = Float.parseFloat(apiData[2].trim()) + durationTemp;											
				id = UUID.randomUUID().toString();
				ApiPerformance apiPerformance = new ApiPerformance(id, iteration, responseTime, duration, releaseCandidateId);
				result = apiDAOImpl.insertApiPerformance(apiPerformance);
				iterationTemp = iteration;
				durationTemp = duration;				
			}
			
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to find the %s api log, due to exception %s", apiLog, e.getMessage()));
		}
		catch (IOException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to read the %s api log, due to exception %s", apiLog, e.getMessage()));
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(String.format("ERROR: Unable to close the %s api log, due to exception %s", apiLog, e.getMessage()));
				}
			}
		}
		return result;
	}
	

}
