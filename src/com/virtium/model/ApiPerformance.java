package com.virtium.model;

public class ApiPerformance {
	
	private String id;
	private int iteration;
	private float responseTime;
	private float duration;
	private String releaseCandidateId;
	
	public ApiPerformance()
	{
		
	}
	
	public ApiPerformance(Integer iteration, Float responseTime, Float duration, String releaseCandidateId)
	{
		this.iteration = iteration;
		this.responseTime = responseTime;
		this.duration = duration;
		this.releaseCandidateId = releaseCandidateId;
	}
	
	public ApiPerformance(String id,Integer iteration, Float responseTime, Float duration, String releaseCandidateId)
	{
		this.id = id;
		this.iteration = iteration;
		this.responseTime = responseTime;
		this.duration = duration;
		this.releaseCandidateId = releaseCandidateId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getIteration() {
		return iteration;
	}

	public void setIteration(int iteration) {
		this.iteration = iteration;
	}

	public float getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(float responseTime) {
		this.responseTime = responseTime;
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	public String getReleaseCandidateId() {
		return releaseCandidateId;
	}

	public void setReleaseCandidateId(String releaseCandidateId) {
		this.releaseCandidateId = releaseCandidateId;
	}

	
	
	

}
