package com.virtium.model;

public class SlatPerformance {
	
	private String id;
	private Integer time;
	private Integer slat;
	private Integer readWrite;
	private String releaseCandidateId;
	
	public SlatPerformance()
	{
		
	}
	public SlatPerformance(Integer time, Integer slat, Integer readWrite, String releaseCandidateId)
	{
		this.time = time;
		this.slat = slat;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public SlatPerformance(String id, Integer time, Integer slat, Integer readWrite, String releaseCandidateId)
	{
		this.id = id;
		this.time = time;
		this.slat = slat;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	public Integer getSlat() {
		return slat;
	}
	public void setSlat(Integer slat) {
		this.slat = slat;
	}
	public Integer getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(Integer readWrite) {
		this.readWrite = readWrite;
	}
	public String getReleaseCandidateId() {
		return releaseCandidateId;
	}
	public void setReleaseCandidateId(String releaseCandidateId) {
		this.releaseCandidateId = releaseCandidateId;
	}
	
}
