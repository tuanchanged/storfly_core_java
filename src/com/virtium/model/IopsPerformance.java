package com.virtium.model;

public class IopsPerformance {
	
	private String id;
	private Integer time;
	private Integer iops;
	private Integer readWrite;
	private String releaseCandidateId;
	
	public IopsPerformance()
	{
		
	}
	public IopsPerformance(Integer time, Integer iops, Integer readWrite, String releaseCandidateId)
	{
		this.time = time;
		this.iops = iops;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public IopsPerformance(String id, Integer time, Integer iops, Integer readWrite, String releaseCandidateId)
	{
		this.id = id;
		this.time = time;
		this.iops = iops;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	public Integer getIops() {
		return iops;
	}
	public void setIops(Integer iops) {
		this.iops = iops;
	}
	public Integer getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(Integer readWrite) {
		this.readWrite = readWrite;
	}
	public String getReleaseCandidateId() {
		return releaseCandidateId;
	}
	public void setReleaseCandidateId(String releaseCandidateId) {
		this.releaseCandidateId = releaseCandidateId;
	}
	
	

}
