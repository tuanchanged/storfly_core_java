package com.virtium.model;

public class LatencyPerformance {
	
	private String id;
	private Integer time;
	private Integer latency;
	private Integer readWrite;
	private String releaseCandidateId;
	
	public LatencyPerformance()
	{
		
	}
	public LatencyPerformance(Integer time, Integer latency, Integer readWrite, String releaseCandidateId)
	{
		this.time = time;
		this.latency = latency;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public LatencyPerformance(String id, Integer time, Integer latency, Integer readWrite, String releaseCandidateId)
	{
		this.id = id;
		this.time = time;
		this.latency = latency;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	public Integer getLatency() {
		return latency;
	}
	public void setLatency(Integer latency) {
		this.latency = latency;
	}
	public Integer getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(Integer readWrite) {
		this.readWrite = readWrite;
	}
	public String getReleaseCandidateId() {
		return releaseCandidateId;
	}
	public void setReleaseCandidateId(String releaseCandidateId) {
		this.releaseCandidateId = releaseCandidateId;
	}
	
	
	

}
