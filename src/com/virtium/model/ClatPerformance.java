package com.virtium.model;

public class ClatPerformance {
	
	private String id;
	private Integer time;
	private Integer clat;
	private Integer readWrite;
	private String releaseCandidateId;
	
	public ClatPerformance()
	{
		
	}
	public ClatPerformance(Integer time, Integer clat, Integer readWrite, String releaseCandidateId)
	{
		this.time = time;
		this.clat = clat;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public ClatPerformance(String id, Integer time, Integer clat, Integer readWrite, String releaseCandidateId)
	{
		this.id = id;
		this.time = time;
		this.clat = clat;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	public Integer getClat() {
		return clat;
	}
	public void setClat(Integer clat) {
		this.clat = clat;
	}
	public Integer getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(Integer readWrite) {
		this.readWrite = readWrite;
	}
	public String getReleaseCandidateId() {
		return releaseCandidateId;
	}
	public void setReleaseCandidateId(String releaseCandidateId) {
		this.releaseCandidateId = releaseCandidateId;
	}
	
	
	

}
