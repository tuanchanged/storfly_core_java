package com.virtium.model;

public class BandWidthPerformance {
	
	private String id;
	private Integer time;
	private Integer bandWidth;
	private Integer readWrite;
	private String releaseCandidateId;
	
	public BandWidthPerformance()
	{
		
	}
	public BandWidthPerformance(Integer time, Integer bandWidth, Integer readWrite, String releaseCandidateId)
	{
		this.time = time;
		this.bandWidth = bandWidth;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public BandWidthPerformance(String id, Integer time, Integer bandWidth, Integer readWrite, String releaseCandidateId)
	{
		this.id = id;
		this.time = time;
		this.bandWidth = bandWidth;
		this.readWrite = readWrite;
		this.releaseCandidateId = releaseCandidateId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getTime() {
		return time;
	}
	public void setTime(Integer time) {
		this.time = time;
	}
	public Integer getBandWidth() {
		return bandWidth;
	}
	public void setBandWidth(Integer bandWidth) {
		this.bandWidth = bandWidth;
	}
	public Integer getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(Integer readWrite) {
		this.readWrite = readWrite;
	}
	public String getReleaseCandidateId() {
		return releaseCandidateId;
	}
	public void setReleaseCandidateId(String releaseCandidateId) {
		this.releaseCandidateId = releaseCandidateId;
	}
	
	
	
	
	
	
	

}
