package com.virtium.dao;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.IopsPerformance;

public class IopsPerformanceDAOImpl implements IopsPerformanceDAO{

	
	private DatabaseConnection db;
	public IopsPerformanceDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	@Override
	public boolean insertIopsPerformance(IopsPerformance iopsPerformance) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into IopsPerformance values('%s','%d', '%d', '%d','%s')", 
				iopsPerformance.getId(), 
				iopsPerformance.getTime(), 				
				iopsPerformance.getIops(),				
				iopsPerformance.getReadWrite(),
				iopsPerformance.getReleaseCandidateId());	
		
		result = db.execute(queryStr);			
		return result;	
	}

}
