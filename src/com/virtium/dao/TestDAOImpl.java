package com.virtium.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.Test;

public class TestDAOImpl implements TestDAO{
	
	private DatabaseConnection db;
	public TestDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	
	@Override
	public boolean insertTest(Test test) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into Test values('%s','%s', '%s')", test.getId(), test.getName(), test.getCategoryId());		
		if(getTest(test.getName(), test.getCategoryId()) == null)
		{
			result = db.execute(queryStr);	
		}
		else
		{
			System.out.println(String.format("The %s test name is exist. Please insert another test name", test.getName()));
		}			
		return result;		
	}

	@Override
	public Test getTest(String testName, String categoryId) {
		// TODO Auto-generated method stub
		try {			
			String queryStr = String.format("select * from Test where Name='%s' and CategoryId='%s'", testName, categoryId);
			ResultSet rs = null;
			
			rs = db.executeQuery(queryStr);
			if(rs.next())
			{
				Test test = new Test();
				test.setId(rs.getObject("Id").toString());
				test.setName(rs.getObject("Name").toString());
				test.setCategoryId(rs.getObject("CategoryId").toString());				
				return test;
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to execute get test element in Test table"));
		}
		return null;
	}

}
