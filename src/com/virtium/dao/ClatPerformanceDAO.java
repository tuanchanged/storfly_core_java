package com.virtium.dao;

import com.virtium.model.ClatPerformance;

public interface ClatPerformanceDAO {
	
	boolean insertClatPerformance(ClatPerformance clatPerformance);

}
