package com.virtium.dao;

import com.virtium.model.LatencyPerformance;

public interface LatencyPerformanceDAO {

	boolean insertLatencyPerformance(LatencyPerformance latencyPerformance);
}
