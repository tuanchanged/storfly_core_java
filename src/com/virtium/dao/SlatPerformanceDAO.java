package com.virtium.dao;

import com.virtium.model.SlatPerformance;

public interface SlatPerformanceDAO {
	boolean insertSlatPerformance(SlatPerformance slatPerformance);

}
