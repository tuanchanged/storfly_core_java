package com.virtium.dao;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.BandWidthPerformance;

public class BandWidthPerformanceDAOImpl implements BandWidthPerformanceDAO{

	private DatabaseConnection db;
	public BandWidthPerformanceDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	@Override
	public boolean insertBandWidthPerformance(BandWidthPerformance bandWidthPerformance) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into BandWidthPerformance values('%s','%d', '%d', '%d','%s')", 
				bandWidthPerformance.getId(), 
				bandWidthPerformance.getTime(), 				
				bandWidthPerformance.getBandWidth(),				
				bandWidthPerformance.getReadWrite(),
				bandWidthPerformance.getReleaseCandidateId());	
		
		result = db.execute(queryStr);			
		return result;	
	}

}
