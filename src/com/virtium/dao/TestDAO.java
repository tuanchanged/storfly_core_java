package com.virtium.dao;

import com.virtium.model.Test;

public interface TestDAO {
	
	boolean insertTest(Test test);
	Test getTest(String testName, String categoryId);

}
