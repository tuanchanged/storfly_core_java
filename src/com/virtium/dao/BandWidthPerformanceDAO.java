package com.virtium.dao;

import com.virtium.model.BandWidthPerformance;

public interface BandWidthPerformanceDAO {
	boolean insertBandWidthPerformance(BandWidthPerformance bandWidthPerformance);

}
