package com.virtium.dao;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.ClatPerformance;

public class ClatPerformanceDAOImpl implements ClatPerformanceDAO{

	private DatabaseConnection db;
	public ClatPerformanceDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	@Override
	public boolean insertClatPerformance(ClatPerformance clatPerformance) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into ClatPerformance values('%s','%d', '%d', '%d','%s')", 
				clatPerformance.getId(), 
				clatPerformance.getTime(), 				
				clatPerformance.getClat(),				
				clatPerformance.getReadWrite(),
				clatPerformance.getReleaseCandidateId());	
		
		result = db.execute(queryStr);			
		return result;	
	}

}
