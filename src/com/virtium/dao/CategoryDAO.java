package com.virtium.dao;

import com.virtium.model.Category;

public interface CategoryDAO {	
	boolean insertCategory(Category category);
	Category getCategory(String categoryName);
}
