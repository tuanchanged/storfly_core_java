package com.virtium.dao;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.SlatPerformance;

public class SlatPerformanceDAOImpl implements SlatPerformanceDAO{

	private DatabaseConnection db;
	public SlatPerformanceDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	@Override
	public boolean insertSlatPerformance(SlatPerformance slatPerformance) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into SlatPerformance values('%s','%d', '%d', '%d','%s')", 
				slatPerformance.getId(), 
				slatPerformance.getTime(), 				
				slatPerformance.getSlat(),				
				slatPerformance.getReadWrite(),
				slatPerformance.getReleaseCandidateId());	
		
		result = db.execute(queryStr);			
		return result;	
	}

}
