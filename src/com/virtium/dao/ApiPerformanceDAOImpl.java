package com.virtium.dao;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.ApiPerformance;

public class ApiPerformanceDAOImpl implements ApiPerformanceDAO{

	private DatabaseConnection db;
	public ApiPerformanceDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	@Override
	public boolean insertApiPerformance(ApiPerformance apiPerformance) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into ApiPerformance values('%s', '%d', '%f', '%f', '%s')", 
				apiPerformance.getId(),
				apiPerformance.getIteration(),
				apiPerformance.getResponseTime(),
				apiPerformance.getDuration(),
				apiPerformance.getReleaseCandidateId());
		result = db.execute(queryStr);
		return result;
	}

}
