package com.virtium.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.Category;

public class CategoryDAOImpl implements CategoryDAO{

	private DatabaseConnection db;
	public CategoryDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	
	@Override
	public Category getCategory(String categoryName) {
		// TODO Auto-generated method stub
		try {
			String queryStr = String.format("select * from Category where Name='%s'", categoryName);
			ResultSet rs = null;
			
			rs = db.executeQuery(queryStr);
			if(rs.next())
			{
				Category category = new Category();
				category.setId(rs.getObject("Id").toString());
				category.setName(rs.getObject("Name").toString());
				return category;
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to execute get category element in Category table"));
		}
		
		return null;
	}
	
	
	
	@Override
	public boolean insertCategory(Category category) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into Category values('%s','%s')", category.getId().toString(), category.getName());
		if(getCategory(category.getName()) == null)
		{
			result = db.execute(queryStr);	
		}
		else
		{
			System.out.println(String.format("The %s category name is exist. Please insert another category name", category.getName()));
		}			
		return result;
	}
	
	

}
