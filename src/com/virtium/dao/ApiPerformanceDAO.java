package com.virtium.dao;

import com.virtium.model.ApiPerformance;

public interface ApiPerformanceDAO {
	boolean insertApiPerformance(ApiPerformance apiPerformance);

}
