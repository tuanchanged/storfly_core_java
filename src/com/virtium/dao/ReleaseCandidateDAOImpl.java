package com.virtium.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.ReleaseCandidate;
import com.virtium.model.Test;

public class ReleaseCandidateDAOImpl implements ReleaseCandidateDAO{

	private DatabaseConnection db;
	
	public ReleaseCandidateDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	@Override
	public boolean insertReleaseCandidate(ReleaseCandidate releaseCandidate) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into ReleaseCandidate values('%s','%s', '%s')", releaseCandidate.getId(), releaseCandidate.getName(), releaseCandidate.getTestId());		
		if(getReleaseCandidate(releaseCandidate.getName(), releaseCandidate.getTestId()) == null)
		{
			result = db.execute(queryStr);	
		}
		else
		{
			System.out.println(String.format("The %s version name is exist. Please insert another version name",releaseCandidate.getName()));
		}			
		return result;
	}

	@Override
	public ReleaseCandidate getReleaseCandidate(String versionName, String testId) {
		// TODO Auto-generated method stub
		try {			
			String queryStr = String.format("select * from ReleaseCandidate where Name='%s' and TestId='%s'", versionName, testId);
			ResultSet rs = null;
			
			rs = db.executeQuery(queryStr);
			if(rs.next())
			{
				ReleaseCandidate releaseCandidate = new ReleaseCandidate();
				releaseCandidate.setId(rs.getObject("Id").toString());
				releaseCandidate.setName(rs.getObject("Name").toString());
				releaseCandidate.setTestId(rs.getObject("TestId").toString());							
				return releaseCandidate;
			}
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to execute get release candidate element in ReleaseCandidate table"));
		}
		return null;
	}

}
