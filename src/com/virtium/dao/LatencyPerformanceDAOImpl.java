package com.virtium.dao;

import com.virtium.database.DatabaseConnection;
import com.virtium.model.LatencyPerformance;

public class LatencyPerformanceDAOImpl implements LatencyPerformanceDAO{

	private DatabaseConnection db;
	public LatencyPerformanceDAOImpl(DatabaseConnection db) {
		// TODO Auto-generated constructor stub
		this.db = db;
	}
	@Override
	public boolean insertLatencyPerformance(LatencyPerformance latencyPerformance) {
		// TODO Auto-generated method stub
		boolean result = false;
		String queryStr = String.format("insert into LatencyPerformance values('%s','%d', '%d', '%d','%s')", 
				latencyPerformance.getId(), 
				latencyPerformance.getTime(), 				
				latencyPerformance.getLatency(),				
				latencyPerformance.getReadWrite(),
				latencyPerformance.getReleaseCandidateId());	
		
		result = db.execute(queryStr);			
		return result;	
	}

}
