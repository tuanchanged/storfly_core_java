package com.virtium.dao;

import com.virtium.model.ReleaseCandidate;

public interface ReleaseCandidateDAO {
	
	boolean insertReleaseCandidate(ReleaseCandidate releaseCandidate);
	ReleaseCandidate getReleaseCandidate(String versionName, String testId);

}
