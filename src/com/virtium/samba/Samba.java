package com.virtium.samba;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

public class Samba {

	private String m_ServerAddress = null;
	private String m_Username = null;
	private String m_Password = null;
	private String m_SharedFolder = null;
	private NtlmPasswordAuthentication m_NtlmPasswordAuthentication = null;	
	
    
	/*******************************************************
	 * Constructor
	 * 
	 * @Author: tuan.nguyen
	 **/
	public Samba() {
	}

	/*******************************************************
	 * Constructor
	 * 
	 * @Author: tuan.nguyen
	 * @param iServerAddress:
	 *            The server address(ip address)
	 * @param iUsername:
	 *            The user name
	 * @param iPassword:
	 *            The password
	 * @param iSharedFolder:
	 *            The shared folder
	 **/
	public Samba(String iServerAddress, String iUsername, String iPassword, String iSharedFolder) {
		m_ServerAddress = iServerAddress;
		m_Username = iUsername;
		m_Password = iPassword;
		m_SharedFolder = iSharedFolder;
		m_NtlmPasswordAuthentication = new NtlmPasswordAuthentication(null, m_Username, m_Password);
		
	}

	/*******************************************************
	 * Get the latest segment of a particular stream
	 * 
	 * @Author: tuan.nguyen
	 * @param iStreamId:
	 *            The streamId
	 **/
	public HashMap<Object, Object> getLatestSegmentViaSamba(String iStreamId) throws UnknownHostException, IOException {

		// Filter stream directory
		String stream_path = String.format("smb://%s/%s/%s/", m_ServerAddress, m_SharedFolder, iStreamId);
		SmbFile streamDir = new SmbFile(stream_path, m_NtlmPasswordAuthentication);

		// Filter latest year directory
		String the_latest_year_dir = getLatestItemAfterSortingByName(streamDir.list());
		String the_latest_year_path = String.format("%s/%s/", stream_path, the_latest_year_dir);
		SmbFile the_latest_year_dir_samba = new SmbFile(the_latest_year_path, m_NtlmPasswordAuthentication);

		// Filter latest month directory
		String the_latest_month_dir = getLatestItemAfterSortingByName(the_latest_year_dir_samba.list());
		String the_latest_month_path = String.format("%s/%s/", the_latest_year_dir_samba, the_latest_month_dir);
		SmbFile the_latest_month_dir_samba = new SmbFile(the_latest_month_path, m_NtlmPasswordAuthentication);

		// Filter latest day directory
		String the_latest_day_dir = getLatestItemAfterSortingByName(the_latest_month_dir_samba.list());
		String the_latest_day_path = String.format("%s/%s/", the_latest_month_path, the_latest_day_dir);
		SmbFile the_latest_day_dir_samba = new SmbFile(the_latest_day_path, m_NtlmPasswordAuthentication);

		// Filter latest segment directory
		String the_latest_segment_dir = getLatestItemAfterSortingByName(the_latest_day_dir_samba.list());
		String the_latest_segment_path = String.format("%s/%s", the_latest_day_path, the_latest_segment_dir);
		SmbFile the_latest_segment_samba = new SmbFile(the_latest_segment_path, m_NtlmPasswordAuthentication);

		// Get latest segment properties
		HashMap<Object, Object> latest_segment = null;
		latest_segment = new HashMap<Object, Object>();
		latest_segment.put("streamId", iStreamId);
		latest_segment.put("segmentId",
				the_latest_segment_samba.getName().substring(0, the_latest_segment_samba.getName().lastIndexOf(".")));
		latest_segment.put("year", the_latest_year_dir);
		latest_segment.put("month", the_latest_month_dir);
		latest_segment.put("day", the_latest_day_dir);
		latest_segment.put("name", the_latest_segment_samba.getName());
		latest_segment.put("size", the_latest_segment_samba.length());
		latest_segment.put("content",
				IOUtils.toString(
						new SmbFileInputStream(new SmbFile(the_latest_segment_path, m_NtlmPasswordAuthentication)),
						StandardCharsets.UTF_8.name()));
		return latest_segment;
	}

	/*******************************************************
	 * Get the oldest segment of a particular stream
	 * 
	 * @Author: tuan.nguyen
	 * @param iStreamId:
	 *            The streamId
	 **/
	public HashMap<Object, Object> getOldestSegmentViaSamba(String iStreamId) throws UnknownHostException, IOException {

		// Filter stream directory
		String stream_path = String.format("smb://%s/%s/%s/", m_ServerAddress, m_SharedFolder, iStreamId);
		SmbFile streamDir = new SmbFile(stream_path, m_NtlmPasswordAuthentication);

		// Filter oldest year directory
		String the_latest_year_dir = getOldestItemAfterSortingByName(streamDir.list());
		String the_latest_year_path = String.format("%s/%s/", stream_path, the_latest_year_dir);
		SmbFile the_latest_year_dir_samba = new SmbFile(the_latest_year_path, m_NtlmPasswordAuthentication);

		// Filter oldest month directory
		String the_latest_month_dir = getOldestItemAfterSortingByName(the_latest_year_dir_samba.list());
		String the_latest_month_path = String.format("%s/%s/", the_latest_year_dir_samba, the_latest_month_dir);
		SmbFile the_latest_month_dir_samba = new SmbFile(the_latest_month_path, m_NtlmPasswordAuthentication);

		// Filter oldest day directory
		String the_latest_day_dir = getOldestItemAfterSortingByName(the_latest_month_dir_samba.list());
		String the_latest_day_path = String.format("%s/%s/", the_latest_month_path, the_latest_day_dir);
		SmbFile the_latest_day_dir_samba = new SmbFile(the_latest_day_path, m_NtlmPasswordAuthentication);

		// Filter oldest segment directory
		String the_latest_segment_dir = getOldestItemAfterSortingByName(the_latest_day_dir_samba.list());
		String the_latest_segment_path = String.format("%s/%s", the_latest_day_path, the_latest_segment_dir);
		SmbFile the_latest_segment_samba = new SmbFile(the_latest_segment_path, m_NtlmPasswordAuthentication);

		// Get oldest segment properties
		HashMap<Object, Object> latest_segment = null;
		latest_segment = new HashMap<Object, Object>();
		latest_segment.put("streamId", iStreamId);
		latest_segment.put("segmentId",
				the_latest_segment_samba.getName().substring(0, the_latest_segment_samba.getName().lastIndexOf(".")));
		latest_segment.put("year", the_latest_year_dir);
		latest_segment.put("month", the_latest_month_dir);
		latest_segment.put("day", the_latest_day_dir);
		latest_segment.put("name", the_latest_segment_samba.getName());
		latest_segment.put("size", the_latest_segment_samba.length());
		latest_segment.put("content",
				IOUtils.toString(
						new SmbFileInputStream(new SmbFile(the_latest_segment_path, m_NtlmPasswordAuthentication)),
						StandardCharsets.UTF_8.name()));
		return latest_segment;
	}

	private String getLatestItemAfterSortingByName(String[] iItems) {
		if (iItems.length >= 1) {
			List<String> list_item = Arrays.asList(iItems);

			if (iItems.length > 1) {
				Collections.sort(list_item);
			}

			return list_item.get(list_item.size() - 1);
		} else {
			return null;
		}
	}

	private String getOldestItemAfterSortingByName(String[] iItems) {
		if (iItems.length >= 1) {
			List<String> list_item = Arrays.asList(iItems);

			if (iItems.length > 1) {
				Collections.sort(list_item);
			}

			return list_item.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Get meta of all streams
	 * 
	 * @author tuan.nguyen
	 **/
	public String[] getMetaOfAllStreams() throws SmbException, MalformedURLException {
		String shared_folder_path = String.format("smb://%s/%s/", m_ServerAddress, m_SharedFolder);
		SmbFile samba_shared_dir = new SmbFile(shared_folder_path, m_NtlmPasswordAuthentication);

		return samba_shared_dir.list();
	}

	public List<String> getAllFileNameInDirViaSamba(String serverAddress, String username, String password,
			String sharedFolder, String subFolder) {
		List<String> listOfFileNames = new ArrayList<String>();
		try {
			String url = String.format("smb://%s/%s/%s/", serverAddress, sharedFolder, subFolder);
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);
			SmbFile baseDir = new SmbFile(url, auth);
			System.out.println("baseDir: " + baseDir);
			SmbFile[] smbFiles = baseDir.listFiles();
			for (int i = 0; i < smbFiles.length; i++) {
				SmbFile smbFile = smbFiles[i];
				if (smbFile.isFile()) {
					listOfFileNames.add(smbFile.getName());
				}
			}
		} catch (Exception e) {
			System.out.println(
					String.format("ERROR: Unable get all filenames via samba, due to excaption %s", e.getMessage()));
		}

		return listOfFileNames;
	}
	
	public void getAllFileNameInStream(String streamPath, List<String> listOfFileNames)
	{
		
		try {
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, m_Username, m_Password);
			SmbFile baseDir = new SmbFile(streamPath, auth);
			SmbFile[] smbFiles = baseDir.listFiles();
			for ( SmbFile f : smbFiles ) {
	            if ( f.isDirectory() ) {
	            	getAllFileNameInStream( f.getCanonicalPath() , listOfFileNames);
	            }
	            else {
	            	System.out.println(String.format("FilePath: %s", f.getCanonicalPath()));
	                listOfFileNames.add(f.getCanonicalPath());
	            }
	        }
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable get all filenames in stream , due to exception %s", e.getMessage()));
			
		}
				
	}
	
	public List<String> getAllSampleInSegment(String segmentPath) {
		List<String> allSamples = new ArrayList<String>();
		String content = null;
		try {
			content = getContentOfSegmentViaSamba(segmentPath);			
			JSONArray sampleArray = new JSONArray(content);												
			for (int index = 0; index < sampleArray.length(); index++) {
				JSONObject dataObj = sampleArray.getJSONObject(0);
				String dataValue = dataObj.getString("data");
//				allSamples.add(sampleArray.get(index).toString());
				allSamples.add(dataValue);
			}
		} catch (Exception ex) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable get all samples in the %s segment, due to exception %s",
					segmentPath, ex.getMessage()));
		}
		return allSamples;
	}
	
		
	public long getSizeFileViaSamba(String serverAddress, String username, String password, String sharedFolder,
			String subFolder, String fileName) {
		long sizeFile = 0;
		try {
			String url = String.format("smb://%s/%s/%s/%s", serverAddress, sharedFolder, subFolder, fileName);
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);
			SmbFile smbFile = new SmbFile(url, auth);
			sizeFile = smbFile.length();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable get size of file, due to exception %s", e.getMessage()));
		}
		return sizeFile;
	}


	public String getContentOfSegmentViaSamba(String segmentPath) {
		String content = null;
		
		try {	
			SmbFile smbFile = new SmbFile(segmentPath, m_NtlmPasswordAuthentication);
			SmbFileInputStream in = new SmbFileInputStream(smbFile);
			content = IOUtils.toString(in);
			in.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable get the content of %s file via samba, due to exception %s",
					segmentPath, e.getMessage()));
		}
		
		return content;
	}

	public String getFileExtensionViaSamba(String fileName) {
		String fileExtension = null;
		try {

			fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
		} catch (Exception ex) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable get about extension of file %s, due to exception ",
					fileName, ex.getMessage()));
		}
		return fileExtension;
	}

	public void mountWindowsDevice(String serverAddress, String username, String password, String sharedFolder) {
		try {
			String command = String.format("C:\\windows\\system32\\net.exe use f: \\\\%s\\%s /user:%s %s",
					serverAddress, sharedFolder, username, password);
			Runtime.getRuntime().exec(command);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(
					String.format("ERROR: Unable mount device to windows, due to exception %s", e.getMessage()));
		}
	}
	
	public boolean writeFileViaSamba(String fileContent, String filePath)
	{
		boolean result = false;
		
		try {
			SmbFile smbFile = new SmbFile(filePath, m_NtlmPasswordAuthentication);
			SmbFileOutputStream smbFileOS = new SmbFileOutputStream(smbFile);
			smbFileOS.write(fileContent.getBytes());			
			smbFileOS.close();
			result = true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to write data to the %s file, due to exception %s", filePath, e.getMessage()));
		}
		return result;
	}
	
	public boolean createFolderViaSamba(String folderPath)
	{
		boolean result = false;
		try {
			SmbFile smbFile = new SmbFile(folderPath, m_NtlmPasswordAuthentication);
			if(!smbFile.exists())
			{
				smbFile.mkdir();
			}	
			
			result = true;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(String.format("ERROR: Unable to create the %s folder, due to exception %s", folderPath, e.getMessage()));
		}
		return result;
	}



}
